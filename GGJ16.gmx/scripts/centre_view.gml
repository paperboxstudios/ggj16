/// centre_view(obj1, obj2, viewnumber);
with (argument0) {
    view_hview[argument2] = max((distance_to_object(argument1)) + 150, 270);
    view_wview[argument2] = view_hview[argument2] * (16/9);
//    view_wview[argument2]=max((distance_to_object(argument1)*2) + 100, 480);

    //view_xview[argument2]=((x+argument1.x)/2)-(view_wview[argument2]/2)//sets the x position of the view
    //view_yview[argument2]=((y+argument1.y)/2)-(view_hview[argument2]/2)// sets the y position of the view
}
