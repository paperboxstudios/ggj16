do {
    targx = x + irandom_range(-200, 200);
    targy = y + irandom_range(-200, 200);
} until (!place_meeting(targx, targy, controller_solid) 
            && targx > 0 && targx < room_width
            && targy > 0 && targy < room_height);
