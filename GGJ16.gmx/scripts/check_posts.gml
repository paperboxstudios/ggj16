postcount = 0;
testcol = colour;

with(oocornerpost) {
    if (colour == other.testcol) {
        other.postcount++;
    }    
}

if (postcount == 4) {
    // use 2 triangles to convert everything inside the 4 posts to the same colour
    with(controller_enemy) {
        if ((point_in_triangle(x, y, global.post1.x, global.post1.y, global.post2.x, global.post2.y, global.post3.x, global.post3.y))
        || (point_in_triangle(x, y, global.post4.x, global.post4.y, global.post2.x, global.post2.y, global.post3.x, global.post3.y))
        || (point_in_triangle(x, y, global.post1.x, global.post1.y, global.post4.x, global.post4.y, global.post3.x, global.post3.y))) {
            enemy_set_colour(other.colour);
        }
    }
    
    with (oocornerpost) {
        colour = c_black;
    }
}
