ds_list_delete(hp, 0);

if (ds_list_size(hp) == 0) {
    add_kill(colour);
    with (ooplayer) {hp++;}
    instance_destroy();
    return 0;
}

size = 1 + ds_list_size(hp) * 0.1;
image_xscale = size;
image_yscale = size;

return ds_list_find_value(hp, 0);
